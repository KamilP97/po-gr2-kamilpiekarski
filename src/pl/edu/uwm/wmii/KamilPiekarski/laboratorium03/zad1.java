package pl.edu.uwm.wmii.KamilPiekarski.laboratorium03;
import java.util.*;
public class zad1 {

    public static int countChar(String str, char c) {
        int wynik=0;
        for (int i = 0; i < str.length(); i++) {
            if(str.charAt(i)==c)wynik++;
        }
        return wynik;
    }


    public static int countSubStr(String str, String subStr) {
        int wynik = 0;
        int id = 0;
        while ((id = str.indexOf(subStr, id)) != -1) {
            id++;
            wynik++;
        }
        return wynik;
    }


    public static String Strmiddle(String str) {
        int position;
        int length;
        if (str.length() % 2 == 0)
        {
            position = str.length() / 2 - 1;
            length = 2;
        }
        else
        {
            position = str.length() / 2;
            length = 1;
        }
        return str.substring(position, position + length);
    }

    public static String repeat(String str, int n) {
        String wynik = "";
        for (int i = 0; i < n; i++) {
            wynik = wynik + str;
        }
        return wynik;
    }

    public static int[] where(String str, String subStr) {
        int ind = 0;
        int rozmiar = 0;
        int[] wynik = new int[rozmiar];
        for (int i = 0; i < str.length(); i++) {
            for (int j = i; j <= str.length(); j++) {
                if (str.substring(i, j).equals(subStr)) {
                    rozmiar++;
                    int[] tmp = new int[rozmiar];
                    for (int k = 0; k < wynik.length; k++) {
                        tmp[k] = wynik[k];
                    }
                    wynik = tmp;
                    wynik[ind] = i;
                    ind++;

                }
            }

        }
        System.out.println(Arrays.toString(wynik));
        return wynik;
    }


    static String change(String str) {
        char[] tmp = str.toCharArray();
        String wynik = "";
        StringBuffer ponka = new StringBuffer();
        for (int i = 0; i < tmp.length; i++) {
            if (Character.isUpperCase(tmp[i])) {
                tmp[i] = Character.toLowerCase(tmp[i]);
            } else {
                tmp[i] = Character.toUpperCase(tmp[i]);
            }
            ponka.append(tmp[i]);
        }
        wynik = ponka.toString();
        return wynik;
    }

    static String nice(String str) {
        String wynik = "";
        StringBuffer nowy=new StringBuffer();
        int ile=0;
        for(int i=str.length()-1;i>=0;i--)
        {
            char tmp=str.charAt(i);
            nowy.append(tmp);
            ile++;
            if(ile%3==0)
            {
                nowy.append('\'');
            }
        }
        nowy.reverse();
        wynik=nowy.toString();
        return wynik;
    }
    static String nice(String str, char c,int coIle) {
        String wynik = "";
        StringBuffer nowy=new StringBuffer();
        int ile=0;
        for(int i=str.length()-1;i>=0;i--)
        {
            char tmp=str.charAt(i);
            nowy.append(tmp);
            ile++;
            if(ile%coIle==0)
            {
                nowy.append(c);
            }
        }
        nowy.reverse();
        wynik=nowy.toString();
        return wynik;
    }


    public static void main(String[] args) {

        System.out.println("Podaj tekst");
        Scanner in=new Scanner(System.in);
        String pobrana=in.next();

        int x= countChar("ala ma kota", 'a');
        System.out.println("ilosc powtorzen:"+x);

        int y =countSubStr("jajajajajja","ja");
        System.out.println("ilość substringa w stringu:"+y);

	    String z= Strmiddle("alalalalalala");
        System.out.println("srodek:"+z);

        System.out.println(repeat(pobrana, 10));

        where("jakkajak","jak");

        System.out.println(change("MeDuZaA"));
        System.out.println(nice("099567878979",'-',8));
    }
}
