package pl.edu.uwm.wmii.KamilPiekarski.laboratorium02;

import java.util.Random;
import java.util.Scanner;

class sprawdzanie {

    public static int[] generuj( int n, int min, int maks) {
        Random generator = new Random();

        int []tab = new int[n];
        if (min<0)min*=-1;
        if (maks<0)maks*=-1;
        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(maks+min) - min;
        }
        return tab;
    }

    public static void parzyste(int[]tab) {
        Random generator = new Random();

        int licznik=0;
        for (int i = 0; i <tab.length ; i++) {
            if(tab[i]%2==0)licznik++;

        }
        System.out.println("liczba parzystych:"+licznik);
    }

    public static void nieparzyste(int[]tab) {
        Random generator = new Random();

        int licznik=0;
        for (int i = 0; i <tab.length ; i++) {
            if(tab[i]%2!=0)licznik++;

        }
        System.out.println("liczba nieparzystych:"+licznik);
    }



}
public class zad2a {

    public static void main(String[] args) {
       int []x= sprawdzanie.generuj(10, 100, 10);
        for (int i=0;i<x.length;i++)
        {
            System.out.print(x[i]+",");
        }
       sprawdzanie.parzyste(x);
       sprawdzanie.nieparzyste(x);
    }
}
