package pl.edu.uwm.wmii.KamilPiekarski.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad1g {

    public static void main(String[] args) {

        Scanner wczyt = new Scanner(System.in);
        Random generator = new Random();
        int n;
        do {
            System.out.println("Podaj dlugosc tablicy");
            ;
            n = wczyt.nextInt();
        }
        while (n < 1 || n > 100);
        int lewy,prawy;

        int[] tab = new int[n];
        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(1999) - 999;
        }

        for (int i = 0; i < n; i++) {
            System.out.print(tab[i] + ",");
        }
        System.out.println();
        do {
            System.out.print("Podaj poczatek odwracania:");

            lewy = wczyt.nextInt();
        }
        while (lewy < 0 || lewy > n-1);
        do {
            System.out.print("Podaj koniec odwracania:");

            prawy = wczyt.nextInt();
        }
        while (prawy < 0 || prawy > n-1);

        while (lewy < prawy) {
              int pomoc = tab[lewy];
              tab[lewy]  = tab[prawy];
              tab[prawy] = pomoc;
              lewy++;
              prawy--;
            }

        System.out.println();
        for (int i = 0; i < n; i++) {
            System.out.print(tab[i] + ",");
        }
        System.out.println();
    }
}