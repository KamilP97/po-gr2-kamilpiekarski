package pl.edu.uwm.wmii.KamilPiekarski.laboratorium02;

import java.util.Random;


class szukanie {

    public static int[] generuj( int n, int min, int maks) {
        Random generator = new Random();

        int []tab = new int[n];
        if (min<0)min*=-1;
        if (maks<0)maks*=-1;
        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(maks+min) - min;
        }
        return tab;
    }

    public static void dodatnie(int[]tab) {
        Random generator = new Random();

        int licznik=0;
        for (int i = 0; i <tab.length ; i++) {
            if(tab[i]>0)licznik++;

        }
        System.out.println("liczba dodatnich:"+licznik);
    }

    public static void ujemne(int[]tab) {
        Random generator = new Random();

        int licznik=0;
        for (int i = 0; i <tab.length ; i++) {
            if(tab[i]<0)licznik++;

        }
        System.out.println("liczba ujemnych:"+licznik);
    }

    public static void zera(int[]tab) {
        Random generator = new Random();

        int licznik=0;
        for (int i = 0; i <tab.length ; i++) {
            if(tab[i]==0)licznik++;

        }
        System.out.println("liczba zer:"+licznik);
    }


}
public class zad2b {

    public static void main(String[] args) {
        int[] x = szukanie.generuj(10, -10, 10);
        szukanie.dodatnie(x);
        szukanie.ujemne(x);
        szukanie.zera(x);

        for (int i=0;i<x.length;i++)
        {
            System.out.print(x[i]+",");
        }
    }
}
