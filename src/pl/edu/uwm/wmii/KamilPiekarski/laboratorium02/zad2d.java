package pl.edu.uwm.wmii.KamilPiekarski.laboratorium02;

import java.util.Random;


class funkcje {

    public static int[] generuj( int n, int min, int maks) {
        Random generator = new Random();

        int []tab = new int[n];
        if (min<0)min*=-1;
        if (maks<0)maks*=-1;
        for (int i = 0; i < n; i++) {
            tab[i] = generator.nextInt(maks+min) - min;
        }
        return tab;
    }



    public static void sumadod(int[]tab) {
        int suma=0;
        for(int i=0;i<tab.length;i++) {

            if(tab[i]>0)suma+=tab[i];
        }
        String wynik = "suma dodatnich to:"+suma;
        System.out.println(wynik);
    }
    public static void sumaujem(int[]tab) {
        int suma=0;
        for(int i=0;i<tab.length;i++) {

            if(tab[i]<0)suma+=tab[i];
        }
        String wynik = "suma ujemnych to:"+suma;
        System.out.println(wynik);
    }



}
public class zad2d {

    public static void main(String[] args) {
        int[] x = funkcje.generuj(10, -10, 10);

        funkcje.sumaujem(x);
        funkcje.sumadod(x);
        for (int i=0;i<x.length;i++)
        {
            System.out.print(x[i]+",");
        }
    }
}
