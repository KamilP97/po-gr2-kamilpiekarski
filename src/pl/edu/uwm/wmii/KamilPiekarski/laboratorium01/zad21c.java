package laboratoria01.src.pl.edu.uwm.wmii.KamilPiekarski.laboratorium01;

import java.util.Scanner;

public class zad21c {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int x = in.nextInt(), licznik = 0, liczba;
        System.out.print("Podawaj liczby: ");
        for (int i = 0; i < x; i++) {
            liczba = in.nextInt();
            if (Math.sqrt(liczba) % 2 == 0) licznik++;
        }

        System.out.println("ilość liczb = " + licznik);
    }
}